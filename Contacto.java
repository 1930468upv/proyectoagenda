
/**
 * Write a description of class Contacto here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Contacto
{
    // instance variables - replace the example below with your own
    private String nombre;
    private String email;
    private String num;
    private Long fdn;


    /**
     * Constructor for objects of class Contacto
     */
    public Contacto()
    {
        // initialise instance variables
        this.nombre = "";
        this.email = "";
        this.num = "";
        this.fdn = null; 
    }

    /**
     * Obteneer el valor de nombre
     * 
     * @param
     * @return     the sum of x and y 
     */
    public String getNombre()
    {
        // put your code here
        return this.nombre;
    }
    
    public String getEmail()
    {    
        return this.email;
    }
    
    public String getNum()
    {
        return this.num;
    }
    
    public Long getFdn()
    {
        return this.fdn;
    }
    
    public void setNombre(String nombre)
    {
        // put your code here
        this.nombre = nombre;
    }
    
    public void setEmail(String email)
    {
        this.email = email;
    }
    
    public void setNum(String num)
    {
        this.num = num;
    }
    
    public void setFdn(Long fdn)
    {
        this.fdn = fdn;
    }
    
    public String toString(){
        return this.nombre + "-" + this.num + "-" + this.email + "-" + this.fdn;
    }
}

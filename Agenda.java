import java.util.*;
/**
 * Write a description of class Agenda here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Agenda
{
    // instance variables - replace the example below with your own
    List <Contacto>agenda;

    /**
     * Constructor for objects of class Agenda
     */
    public Agenda()
    {
        agenda = new <Contacto> ArrayList();
    }

    /**
     * An example of a method - replace this comment with your own
     * 
     * @param  y   a sample parameter for a method
     * @return     the sum of x and y 
     */
    public void getContactos(){
        Iterator it;
        it = agenda.iterator();
        while(it.hasNext()){
          Contacto contacto = (Contacto)it.next();
          System.out.println("Contacto: " + contacto.getNombre() + " Numero: " + contacto.getNum());
          
        }
    }
    
    public Contacto  buscarContacto(String num){
        Iterator it = agenda.iterator();
        Contacto contacto;
        while(it.hasNext()){
            contacto = (Contacto)it.next();
            if (contacto.getNum().equals(num)){
                return contacto;
            }    
        } 
        return null;
    }
    
    public List<Contacto> getAgenda(){
        return this.agenda;
    }
    
    public void removeContacto(int indexOf){
        /*Iterator it = agenda.iterator();
        Contacto contacto;
        int indexOf;
        while(it.hasNext()){
            contacto = (Contacto)it.next();
            if(contacto.getNum().equals(num)){
                indexOf = agenda.indexOf(contacto);
                agenda.remove(indexOf);
                System.out.println("Contacto eliminado");
            }
        }*/
        agenda.remove(indexOf);
        
    }
    
    public void updateContacto(int index, Contacto contacto){
        agenda.set(index, contacto);
    }
    
    public void agregarContacto(Contacto contacto)
    {
        agenda.add (contacto);
    }
    
    public void guardarAgenda(){
        System.out.println("se guardo agenda ...");
    }
}
